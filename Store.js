import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import rootReducer from '../AwesomeTSProject/src/Reducers/Index';
let Reactotron = null;
if (__DEV__) {
  Reactotron = require('./ReactotronConfig').default;
}

const createStoreWithMW = applyMiddleware(thunk)(createStore);

let store;
if (__DEV__) {
  store = createStoreWithMW(rootReducer, Reactotron.createEnhancer());
} else {
  store = createStoreWithMW(rootReducer);
}

export default store;
