{
  /* <Item style={styles.userStyle} rounded last>
                <Input style={{color: '#ffffff'}} placeholder="Username" />
              </Item>
              <Item style={styles.passStyle} rounded last>
                <Input style={{color: '#ffffff'}} placeholder="Password" />
              </Item>

import React from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Content,
  Item,
  Input,
  Button,
  Text,
  Thumbnail,
} from 'native-base';
import {Formik} from 'formik';
import {useNavigation} from '@react-navigation/native';
import CustomInput from '../Fields/CustomInput';
import {connect} from 'react-redux';
import {login} from '../Actions/auth.login';

const validation = values => {
  let errors = {};

  if (!values.username) {
    errors.username = 'Required';
  } else if (values.username.length > 100) {
    errors.username = 'Must be 15 characters or less';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length > 100) {
    errors.password = 'Must be 15 characters or less';
  }

  return errors;
};

const LoginPage = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Thumbnail
          square
          style={{width: 350, height: 90}}
          source={require('../Images/1484031896-64-devtac-crm-inc.png')}
        />
      </Header>
      <Content>
        <Formik
          initialValues={{
            username: '',
            password: '',
          }}
          onSubmit={values => {
            console.log(values);
            setLoading(true);
            navigation.navigate('MenuPage', {
              username: values.username,
              password: values.password,
            });
          }}
          validate={validation}>
          {({handleSubmit}) => (
            <>
              <CustomInput name="username" label="Username" />
              <CustomInput name="password" label="Password" />
              <Button
                style={styles.buttonStyle}
                full
                rounded
                dark
                onPress={handleSubmit}>
                <Text>Login</Text>
              </Button>
            </>
          )}
        </Formik>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    height: 100,
    backgroundColor: '#ffffff',
  },

  buttonStyle: {
    height: 50,
    backgroundColor: '#1565c0',
    marginTop: 50,
    marginRight: 70,
    marginLeft: 70,
  },

  userStyle: {
    marginTop: 100,
    marginRight: 30,
    marginLeft: 30,
    backgroundColor: '#212121',
  },

  passStyle: {
    marginTop: 10,
    marginRight: 30,
    marginLeft: 30,
    backgroundColor: '#212121',
  },
});

const mapDispatch = dispatch => {
  return {
    loggedin: values => {
      dispatch(login(values));
    },
  };
};
const mapState = state => {
  return {
    loggedin: state.login.loggedin,
  };
};

export default connect(mapState, mapDispatch)(LoginPage); */
}

//this one is for the app.js
import React from 'react';
import 'react-native-gesture-handler';
import Routes from './Routes/R';
import {Provider} from 'react-redux';
import store from './Store';

const App = () => {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
};

export default App;
