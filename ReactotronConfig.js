import { Platform, NativeModules } from 'react-native';
import Reactotron, { asyncStorage } from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';

let config = {
  name: 'React Native Demo"',
};

let scriptHostname = null;

if (__DEV__) {
  const scriptURL = NativeModules.SourceCode.scriptURL;

  if (scriptURL) {
    scriptHostname = scriptURL.split('://')[1].split(':')[0];
  }
}

if (Platform.OS === 'ios' && __DEV__ && scriptHostname !== null) {
  config.host = scriptHostname;
}

const reactotron = Reactotron.configure(config)
  .useReactNative({
    networking: {
      // optionally, you can turn it off with false.
      ignoreUrls: /localhost/,
    },
  })
  .use(reactotronRedux())
  .use(asyncStorage());

if (__DEV__ && scriptHostname !== null) {
  reactotron.connect();
}

export default reactotron;
