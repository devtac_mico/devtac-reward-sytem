import React from 'react';
import {connect} from 'react-redux';
import {Text} from 'native-base';
import {Dialog} from 'react-native-simple-dialogs';
import {useNavigation} from '@react-navigation/native';

const LoginError = ({err, hideDialog}: Props) => {
  const navigation = useNavigation();

  return (
    <Dialog visible={err.errorLogin} title="" onTouchOutside={hideDialog}>
      <Text>{err.errorMessage}</Text>
    </Dialog>
  );
};

const mapstate = state => {
  return {
    err: state.login,
  };
};

const mapDispatch = dispatch => {
  return {
    hideDialog: () => {
      dispatch({
        type: 'HIDE_ERROR',
      });
    },
  };
};

export default connect(mapstate, mapDispatch)(LoginError);
