import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Header, Body, Button, Thumbnail, Text} from 'native-base';
import {useNavigation} from '@react-navigation/native';

const MenuPage = () => {
  const navigation = useNavigation();
  return (
    <Container style={styles.containerStyle}>
      <Header />
      <Body>
        <Button
          style={styles.b1tStyle}
          onPress={() => {
            navigation.navigate('AwardsPage', {});
          }}>
          <Thumbnail
            style={styles.pngtStyle}
            square
            large
            source={require('../Images/bjjgifdogjz.gif')}
          />
        </Button>
        <Text>AWARDS</Text>
        <Button
          style={styles.b1tStyle}
          onPress={() => {
            navigation.navigate('VotePage', {});
          }}>
          <Thumbnail
            style={styles.pngtStyle}
            square
            large
            source={require('../Images/source.gif')}
          />
        </Button>
        <Text>VOTE</Text>
        <Button
          style={styles.b2tStyle}
          onPress={() => {
            navigation.navigate('ViewProfile', {});
          }}>
          <Thumbnail
            style={styles.pngtStyle}
            square
            large
            source={require('../Images/image02.gif')}
          />
        </Button>
        <Text>PROFILE</Text>
      </Body>
      <Button
        block
        style={styles.bltStyle}
        onPress={() => {
          navigation.goBack({});
        }}>
        <Text>Logout</Text>
      </Button>
    </Container>
  );
};

const styles = StyleSheet.create({
  b1tStyle: {
    marginTop: 20,
    width: 150,
    height: 150,
    borderRadius: 100,
    backgroundColor: '#e0e0e0',
  },

  b2tStyle: {
    width: 150,
    height: 150,
    borderRadius: 100,
    backgroundColor: '#e0e0e0',
  },

  bltStyle: {
    marginBottom: 10,
    marginRight: 20,
    marginLeft: 20,
  },

  pngtStyle: {
    height: 90,
    width: 90,
    marginLeft: 32,
  },

  containerStyle: {
    backgroundColor: '#ffffff',
  },
});

export default MenuPage;
