import axios from 'axios';
import URL from '../Config/index';

export const profile = values => async (dispatch, getState) => {
  dispatch({
    type: 'SET_USERS',
    load: true,
  });

  const login = getState().login;

  console.log('getState().login');
  console.log(login.data.user_data.id);

  console.log(login.data.token);

  const config = {
    method: 'get',
    url: URL + 'me/profile/' + login.data.user_data.id,
    headers: {
      Authorization: 'Bearer ' + login.data.token,
      'Content-Type': 'application/json',
    },
  };

  try {
    const response = await axios(config);
    console.log(response.data);
    dispatch({
      type: 'SET_ID',
      load: true,
    });
  } catch (err) {
    console.log(err.response.data.message);
  }
};
