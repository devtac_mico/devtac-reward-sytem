import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import URL from '../Config/index';

export const login = values => async dispatch => {
  dispatch({
    type: 'SET_LOGIN',
    load: true,
  });

  dispatch({
    type: 'SET_LOADING',
    payload: true,
  });

  dispatch({
    type: 'SET_LOGGEDIN',
    load: true,
  });

  const deviceId = DeviceInfo.getDeviceId();
  const data = {
    username: values.username,
    password: values.password,
    device_name: deviceId,
  };

  const config = {
    method: 'post',
    url: URL + 'authenticate/login',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };

  try {
    const response = await axios(config);
    console.log(response.data);

    dispatch({
      type: 'SET_DATA',
      payload: response.data.data.data,
    });

    dispatch({
      type: 'SET_LOADING_SUCCESS',
      load: true,
    });
  } catch (err) {
    console.log(err.response.data.message);
    dispatch({
      type: 'SET_LOGIN_ERROR',
      payload: err.response.data.message,
    });
  }

  dispatch({
    type: 'SET_LOADING',
    payload: false,
  });
};
