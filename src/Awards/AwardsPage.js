import React from 'react';
import {
  Container,
  Form,
  Header,
  Content,
  Title,
  Left,
  Right,
  Card,
  Thumbnail,
  Text,
  View,
} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import CustomPicker from '../Fields/CustomPicker';
import {Formik} from 'formik';
import {StyleSheet} from 'react-native';

const AwardsPage = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Text
          style={{
            fontWeight: 'bold',
            marginTop: 10,
            fontSize: 50,
          }}>
          AWARDS
        </Text>
      </Header>
      <Content>
        <Formik
          initialValues={{
            month: '',
          }}
          onSubmit={() => {}}>
          {({handleSubmit}) => (
            <>
              <Card style={styles.maincardStyle}>
                <Form style={styles.pickerStyle}>
                  <CustomPicker name="month" label="Month" />
                  <View>
                    <Thumbnail
                      style={styles.image1Style}
                      square
                      large
                      source={require('../Images/giphy.gif')}
                    />
                    <Text style={{marginLeft: 35, fontWeight: 'bold'}}>
                      🥇1st
                    </Text>
                  </View>
                  <View>
                    <Thumbnail
                      style={styles.image2Style}
                      square
                      large
                      source={require('../Images/giphy.gif')}
                    />
                  </View>
                  <View>
                    <Text style={{marginLeft: 163, fontWeight: 'bold'}}>
                      🥈2nd
                    </Text>
                    <Thumbnail
                      square
                      style={styles.image3Style}
                      large
                      source={require('../Images/giphy.gif')}
                    />
                  </View>
                  <Text style={{marginLeft: 305, fontWeight: 'bold'}}>
                    🥉3rd
                  </Text>
                  <Card style={styles.cardStyle}>
                    <Text>Username</Text>
                  </Card>

                  <Card style={styles.card2Style}>
                    <Text>Comments</Text>
                  </Card>
                </Form>
              </Card>
            </>
          )}
        </Formik>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFFFFF',
    elevation: 0,
    shadowOpacity: 0,
    marginBottom: -15,
    height: 90,
  },

  image1Style: {
    marginTop: 10,
    marginBottom: 16,
    marginLeft: 20,
    width: 80,
    height: 80,
    borderRadius: 40,
    overflow: 'hidden',
  },

  image2Style: {
    marginTop: 5,
    marginBottom: 16,
    marginLeft: 150,
    width: 80,
    height: 80,
    borderRadius: 40,
    overflow: 'hidden',
  },

  image3Style: {
    marginTop: 5,
    marginBottom: 16,
    marginLeft: 290,
    width: 80,
    height: 80,
    borderRadius: 40,
    overflow: 'hidden',
  },

  maincardStyle: {
    borderTopWidth: 0,
    elevation: 0,
    shadowOpacity: 0,
  },

  cardStyle: {
    height: 50,
    marginLeft: 50,
    marginRight: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#dcedc8',
  },

  card2Style: {
    height: 200,
    marginLeft: 50,
    marginRight: 50,
    marginBottom: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#dcedc8',
  },

  pickerStyle: {
    marginTop: 55,
  },
});

export default AwardsPage;
