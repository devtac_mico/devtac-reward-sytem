const initialState = {
  authenticating: false,
  isSuccessLogin: null,
  errorLogin: false,
};

type ActionType = {
  type: String,
  payload: any,
};

export default (state = initialState, action: ActionType) => {
  switch (action.type) {
    case 'SET_AUTHENTICATING':
      return {...state, authenticating: action.payload};

    case 'SET_AUTH_ERROR':
      return {
        ...state,
        authenticating: false,
        errorLogin: true,
        errorMessage: action.payload,
      };

    default:
      return {...state};
  }
};
