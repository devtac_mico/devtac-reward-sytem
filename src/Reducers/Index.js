import {combineReducers} from 'redux';
import login from '../Reducers/login.reducer';

const rootReducer = combineReducers({
  login,
});

export default rootReducer;
