const initialState = {
  login: false,
  loggedin: false,
  success: false,
  errorLogin: false,
  errorMessage: '',
  loading: false,
  data: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_LOGIN':
      return {...state, login: action.load};

    case 'SET_LOGGEDIN':
      return {...state, loggedin: action.load};

    case 'SET_LOADING_SUCCESS':
      return {...state, success: action.load};

    case 'SET_LOGIN_ERROR':
      return {...state, errorLogin: true, errorMessage: action.payload};

    case 'HIDE_ERROR':
      return {...state, errorLogin: false};

    case 'SET_LOADING':
      return {...state, loading: action.payload};

    case 'SET_DATA':
      return {...state, data: action.payload};

    default:
      return {...state};
  }
};
