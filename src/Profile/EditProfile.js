import React from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Text,
  Content,
  Form,
  Item,
  Input,
  Button,
  Footer,
  FooterTab,
  Right,
  Left,
} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import CustomInput from '../../src/Fields/CustomInput';
import {Formik} from 'formik';

const EditProfile = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Text
          style={styles.textStyle}
          style={{
            fontWeight: 'bold',
            marginTop: 20,
            fontSize: 50,
          }}>
          Edit Profile
        </Text>
      </Header>
      <Content>
        <Formik
          initialValues={{
            username: 'admin',
            password: 'admin',
          }}
          onSubmit={values => {
            login(values);
          }}>
          <Form>
            <CustomInput name="current_username" label="Current Username" />

            <CustomInput name="current_position" label="Current Position" />

            <Text
              style={{
                fontWeight: 'bold',
                marginTop: 20,
                fontSize: 30,
                alignSelf: 'center',
                alignItems: 'center',
              }}>
              CHANGE PASSWORD
            </Text>

            <CustomInput name="current_password" label="Current Password" />

            <CustomInput name="new_password" label="New Password" />
            <CustomInput name="confirm_password" label="Confirm Password" />
          </Form>
        </Formik>
      </Content>
      <Footer style={styles.ftrStyle}>
        <FooterTab style={{backgroundColor: '#FFFFFF'}}>
          <Button
            full
            rounded
            dark
            onPress={() => {
              navigation.navigate('ViewProfile', {});
            }}
            style={styles.buttonStyle}>
            <Text>Save</Text>
          </Button>
          <Button full rounded dark style={styles.buttonStyle}>
            <Text>Cancel</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFFFFF',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: 100,
    elevation: 0,
    shadowOpacity: 0,
  },

  buttonStyle: {backgroundColor: '#1565c0'},

  ftrStyle: {
    backgroundColor: '#FFFFFF',
    marginTop: 30,
    marginRight: 50,
    marginLeft: 50,
    marginBottom: 10,
    elevation: 0,
    shadowOpacity: 0,
  },

  textStyle: {
    marginTop: 20,
    marginLeft: 65,
  },
});

export default EditProfile;
