import React from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Body,
  Button,
  Thumbnail,
  Text,
  List,
  ListItem,
  Left,
  Card,
  Content,
  View,
} from 'native-base';
import {useNavigation} from '@react-navigation/native';

const ViewProfile = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Text
          style={{
            fontWeight: 'bold',
            marginTop: 20,
            fontSize: 50,
          }}>
          PROFILE
        </Text>
      </Header>

      <List>
        <ListItem thumbnail style={styles.listStyle}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 40,
              overflow: 'hidden',
              marginLeft: 50,
              marginRight: 50,
            }}>
            <Left>
              <Thumbnail square large source={require('../Images/giphy.gif')} />
            </Left>
          </View>
          <Body>
            <Text style={styles.textStyle}>Pablo Escobar</Text>
            <Text note numberOfLines={1} style={styles.textStyle}>
              Keyboard Warrior
            </Text>
          </Body>
        </ListItem>
      </List>

      <Card style={styles.cardSyle}>
        <ListItem>
          <Text style={styles.text2Style}>Gustavo de Jesús Gaviria Rivero</Text>
        </ListItem>
        <ListItem>
          <Text style={styles.text2Style}>Jose Gonzalo Rodriguez Gacha</Text>
        </ListItem>
        <ListItem>
          <Text style={styles.text2Style}>Fabio Ochoa Vásquez</Text>
        </ListItem>
      </Card>
      <Content />

      <Button
        onPress={() => {
          navigation.navigate('EditProfile', {});
        }}
        full
        rounded
        style={styles.editStyle}>
        <Text>Edit</Text>
      </Button>
    </Container>
  );
};

const styles = StyleSheet.create({
  listStyle: {
    height: 120,
  },

  headerStyle: {
    backgroundColor: '#FFFFFF',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: 100,
  },

  textStyle: {},

  text2Style: {
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: '#b3e5fc',
    height: 25,
    marginLeft: 50,
    marginRight: 50,
    flexDirection: 'row',
    elevation: 0,
    shadowOpacity: 0,
  },

  cardSyle: {
    marginTop: 25,
    backgroundColor: '#b3e5fc',
    marginLeft: 50,
    marginRight: 50,
  },

  editStyle: {
    height: 50,
    backgroundColor: '#1565c0',
    marginTop: 50,
    marginRight: 70,
    marginLeft: 70,
    marginBottom: 10,
  },
});

export default ViewProfile;
