import React from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Body,
  Button,
  Thumbnail,
  Text,
  Card,
  Left,
  Right,
} from 'native-base';
import {useNavigation} from '@react-navigation/native';

const VotePage = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Text
          style={{
            fontWeight: 'bold',
            marginTop: 20,
            fontSize: 50,
          }}>
          VOTE
        </Text>
      </Header>
      <Body>
        <Button
          style={styles.b1tStyle}
          onPress={() => {
            navigation.navigate('PointsView', {});
          }}>
          <Thumbnail
            style={styles.pngtStyle}
            square
            large
            source={require('../Images/Coin-icon.png')}
          />
        </Button>
        <Text
          style={{
            fontWeight: 'bold',
          }}>
          1 Point
        </Text>
        <Button
          style={styles.b1tStyle}
          onPress={() => {
            navigation.navigate('PointsView', {});
          }}>
          <Thumbnail
            style={styles.pngtStyle}
            square
            large
            source={require('../Images/Coins-3-icon.png')}
          />
        </Button>
        <Text
          style={{
            fontWeight: 'bold',
          }}>
          2 Points
        </Text>
      </Body>

      <Card style={styles.cardStyle}>
        <Text>.---------------DAYS LEFT TO VOTE---------------.</Text>
      </Card>
    </Container>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFFFFF',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: 100,
  },

  b1tStyle: {
    marginTop: 20,
    width: 150,
    height: 150,
    borderRadius: 100,
    backgroundColor: '#b2dfdb',
  },
  cardStyle: {
    height: 50,
    marginLeft: 50,
    marginRight: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#b2ebf2',
  },

  bltStyle: {
    marginBottom: 10,
    marginRight: 20,
    marginLeft: 20,
  },

  pngtStyle: {
    height: 70,
    width: 70,
    marginLeft: 40,
  },
});

export default VotePage;
