import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Text,
  Button,
  Thumbnail,
} from 'native-base';

import {ImageBackground} from 'react-native';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const ConfirmationScreen = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header />
      <Content>
        <ImageBackground
          source={require('../../../Images/confetti.gif')}
          style={{
            width: '100%',
            height: 350,
            marginTop: 50,
            textAlign: 'center',
          }}
          imageStyle={{resizeMode: 'stretch'}}>
          <Body>
            <Text
              style={{
                fontWeight: 'bold',
                marginTop: 100,
                fontSize: 30,
              }}>
              CONGRATULATIONS!
            </Text>
          </Body>
        </ImageBackground>

        <Button
          style={styles.b1tStyle}
          onPress={() => {
            navigation.navigate('MenuPage', {});
          }}>
          <Thumbnail
            style={styles.pngtStyle}
            square
            large
            source={require('../../../Images/Yes-icon.png')}
          />
        </Button>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  b1tStyle: {
    marginTop: 20,
    marginLeft: 120,
    width: 150,
    height: 150,
    borderRadius: 100,
    backgroundColor: '#b2dfdb',
  },
  pngtStyle: {
    height: 90,
    width: 90,
    marginLeft: 32,
  },

  cardStyle: {
    elevation: 0,
    shadowOpacity: 0,
  },
});

export default ConfirmationScreen;
