import React from 'react';
import {
  Container,
  Form,
  Header,
  Content,
  Title,
  Body,
  Button,
  Card,
  Item,
  Input,
  Text,
} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import CustomEmployeePicker from '../../Fields/CustomEmployeePicker';
import {Formik} from 'formik';
import {StyleSheet} from 'react-native';

const PointsView = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Text
          style={{
            fontWeight: 'bold',
            marginTop: 20,
            fontSize: 50,
          }}>
          Points View
        </Text>
      </Header>
      <Content>
        <Formik
          initialValues={{
            month: '',
          }}
          onSubmit={() => {}}>
          {({handleSubmit}) => (
            <>
              <Card style={styles.card1Style}>
                <Card style={styles.dateStyle}>
                  <Text>DATE</Text>
                </Card>

                <Form>
                  <CustomEmployeePicker name="employee" label="Employee" />

                  <Card style={styles.dateStyle}>
                    <Text>Username</Text>
                  </Card>

                  <Card style={styles.card2Style}>
                    <Item>
                      <Input placeholder="Comments" />
                    </Item>
                  </Card>
                </Form>
                <Button
                  style={styles.buttonStyle}
                  full
                  rounded
                  dark
                  onPress={() => {
                    navigation.navigate('ConfirmationScreen', {});
                  }}>
                  <Text>Submit</Text>
                </Button>
              </Card>
            </>
          )}
        </Formik>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFFFFF',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: 100,
  },

  card1Style: {
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },

  dateStyle: {
    height: 50,
    marginLeft: 50,
    marginRight: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#b3e5fc',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },

  buttonStyle: {
    height: 50,
    backgroundColor: '#1565c0',
    marginTop: 30,
    marginRight: 70,
    marginLeft: 70,
    marginBottom: 70,
  },

  card2Style: {
    height: 200,
    marginLeft: 50,
    marginBottom: 50,
    marginRight: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#b3e5fc',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },
});

export default PointsView;
