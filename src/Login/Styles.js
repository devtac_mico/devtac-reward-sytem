import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  headerStyle: {
    marginTop: 50,
    marginBottom: 50,
    height: 100,
    backgroundColor: '#ffffff',
    elevation: 0,
    shadowOpacity: 0,
  },

  buttonStyle: {
    height: 50,
    backgroundColor: '#1565c0',
    marginTop: 50,
    marginRight: 70,
    marginLeft: 70,
    marginBottom: 50,
    borderRadius: 5,
  },

  cardtStyle: {
    marginLeft: 30,
    marginRight: 30,
  },
});

export default styles;
