import React from 'react';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Thumbnail,
  Spinner,
  Card,
} from 'native-base';
import {Formik} from 'formik';
import {useNavigation} from '@react-navigation/native';
import CustomInput from '../Fields/CustomInput';
import {connect} from 'react-redux';
import {login} from '../Actions/auth.login';
import {ConfirmDialog} from 'react-native-simple-dialogs';
import LoginError from '../Component/LoginError';
import styles from './Styles';

const validation = values => {
  let errors = {};

  if (!values.username) {
    errors.username = 'Please enter your username';
  }

  if (!values.password) {
    errors.password = 'Please enter your password';
  }

  return errors;
};

const LoginPage = ({login, success, hidedialog, loading}) => {
  const navigation = useNavigation();

  return (
    <Container>
      <Header style={styles.headerStyle}>
        <Thumbnail
          square
          style={{width: 350, height: 90}}
          source={require('../Images/1484031896-64-devtac-crm-inc.png')}
        />
      </Header>

      <Content>
        <Formik
          initialValues={{
            username: '',
            password: '',
          }}
          onSubmit={values => {
            login(values);
          }}
          validate={validation}>
          {({handleSubmit}) => (
            <>
              <Card style={styles.cardtStyle}>
                {loading === true && <Spinner color="#1565c0" />}

                <ConfirmDialog
                  title="WELCOME!"
                  visible={success}
                  positiveButton={{
                    title: 'OK',
                    onPress: () => {
                      hidedialog();
                      setTimeout(() => {
                        navigation.navigate('MenuPage', {});
                      }, 1000);
                    },
                  }}
                />

                <LoginError />

                <CustomInput name="username" label="Username" />

                <CustomInput
                  name="password"
                  label="Password"
                  secureTextEntry={true}
                />

                <Button
                  style={styles.buttonStyle}
                  full
                  dark
                  onPress={handleSubmit}>
                  <Text>Login</Text>
                </Button>
              </Card>
            </>
          )}
        </Formik>
      </Content>
    </Container>
  );
};

const mapDispatch = dispatch => {
  return {
    login: values => {
      dispatch(login(values));
    },

    hidedialog: () => {
      dispatch({
        type: 'SET_LOADING_SUCCESS',
        load: false,
      });
    },
  };
};
const mapState = state => {
  return {
    loading: state.login.loading,
    login: state.login.login,
    loggedin: state.login.loggedin,
    success: state.login.success,
  };
};

export default connect(mapState, mapDispatch)(LoginPage);
