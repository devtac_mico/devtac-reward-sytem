import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import 'react-native-gesture-handler';
import React from 'react';
import LoginPage from '../Login/LoginPage';
import MenuPage from '../Menu/MenuPage';
import ViewProfile from '../Profile/ViewProfile';
import EditProfile from '../Profile/EditProfile';
import AwardsPage from '../Awards/AwardsPage';
import VotePage from '../Vote/VotePage';
import PointsView from '../Vote/PointSystem/PointsView';
// import ConfirmationScreen from '../Vote/PointSystem/ConfirmationScreen/';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{headerShown: false}}
          initialRouteName="Login">
          <Stack.Screen name="Login" component={LoginPage} />
          <Stack.Screen name="MenuPage" component={MenuPage} />
          <Stack.Screen name="ViewProfile" component={ViewProfile} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="AwardsPage" component={AwardsPage} />
          <Stack.Screen name="VotePage" component={VotePage} />
          <Stack.Screen name="PointsView" component={PointsView} />
          {/* <Stack.Screen
            name="ConfirmationScreen"
            component={ConfirmationScreen}
          /> */}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default Routes;
