import React from 'react';
import {Item, Input, Label, Text, Picker} from 'native-base';
import {useField} from 'formik';
import {StyleSheet} from 'react-native';

const CustomEmployeePicker = props => {
  const [field, meta, helpers] = useField(props);

  const {setValue} = helpers;

  return (
    <>
      <Item style={styles.pickerStyle}>
        <Picker
          note
          mode="dropdown"
          style={{width: 130, height: 60}}
          selectedValue={field.value}
          onValueChange={value => {
            setValue(value);
          }}>
          <Picker.Item label="Employee" />
          <Picker.Item label="Pablo Escobar" value="1" />
          <Picker.Item label="Mico Martija" value="2" />
          <Picker.Item label="José Santacruz Londoño" value="3" />
          <Picker.Item label="Gustavo de Jesús Gaviria Rivero" value="4" />
          <Picker.Item label="Los Pepes" value="5" />
          <Picker.Item label="Juan David Ochoa Vásquez" value="6" />
          <Picker.Item label="Carlos Lehder" value="7" />
          <Picker.Item label="Griselda Blanco" value="8" />
          <Picker.Item label="Jorge Luis Ochoa Vásquez" value="9" />
          <Picker.Item label="Gilberto Rodríguez Orejuela" value="10" />
          <Picker.Item label="Fabio Ochoa Vásquez" value="11" />
          <Picker.Item label="Miguel Rodríguez Orejuela" value="12" />
        </Picker>
      </Item>
      {meta.touched && meta.error && <Text>{meta.error}</Text>}
    </>
  );
};

const styles = StyleSheet.create({
  pickerStyle: {
    height: 50,
    marginLeft: 50,
    marginRight: 50,
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0,
    backgroundColor: '#e0e0e0',
  },
});

export default CustomEmployeePicker;
