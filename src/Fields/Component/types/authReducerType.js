export type AuthReducerType = {
  authenticating: boolean,
  isSuccessLogin: boolean | any,
  errorLogin: boolean,
  errorMessage: any,
  user: any,
};
