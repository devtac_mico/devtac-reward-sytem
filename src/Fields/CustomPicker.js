import React from 'react';
import {Item, Input, Label, Text, Picker} from 'native-base';
import {useField} from 'formik';

const CustomPicker = props => {
  const [field, meta, helpers] = useField(props);

  const {setValue} = helpers;

  return (
    <>
      <Item>
        <Picker
          note
          mode="dropdown"
          style={{width: 130, height: 60}}
          selectedValue={field.value}
          onValueChange={value => {
            setValue(value);
          }}>
          <Picker.Item label="Month" />
          <Picker.Item label="January" value="1" />
          <Picker.Item label="February" value="2" />
          <Picker.Item label="March" value="3" />
          <Picker.Item label="April" value="4" />
          <Picker.Item label="May" value="5" />
          <Picker.Item label="June" value="6" />
          <Picker.Item label="July" value="7" />
          <Picker.Item label="August" value="8" />
          <Picker.Item label="September" value="9" />
          <Picker.Item label="October" value="10" />
          <Picker.Item label="November" value="11" />
          <Picker.Item label="December" value="12" />
        </Picker>
      </Item>
      {meta.touched && meta.error && <Text>* {meta.error}</Text>}
    </>
  );
};

export default CustomPicker;
