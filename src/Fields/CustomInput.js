import React from 'react';
import {Item, Input, Label, Text} from 'native-base';
import {useField} from 'formik';
import {StyleSheet} from 'react-native';

const CustomInput = props => {
  const [field, meta, helpers] = useField(props);

  const {setValue} = helpers;

  let secureTextEntry = false;
  let otherProps = {};

  if (props.secureTextEntry) {
    secureTextEntry = true;
  }

  if (meta.touched && meta.error) {
    otherProps.error = true;
  }

  return (
    <>
      <Item last style={styles.tfieldStyle} floatingLabel {...otherProps}>
        <Label>{props.label}</Label>
        <Input
          style={styles.textStyle}
          value={field.value}
          onChangeText={text => {
            setValue(text);
          }}
          secureTextEntry={secureTextEntry}
        />
      </Item>
      {meta.touched && meta.error && (
        <Text style={{textAlign: 'center', color: 'red'}}>{meta.error}</Text>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  tfieldStyle: {
    marginLeft: 50,
    marginRight: 50,
    marginTop: 20,
    height: 50,
    backgroundColor: '#e0e0e0',
    borderRadius: 5,
  },

  textStyle: {
    marginLeft: 10,
  },
});

export default CustomInput;
